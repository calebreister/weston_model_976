Weston Model 976 Instruction Manual
===================================

> The [[Weston](http://weston.ftldesign.com/index.html)] Model 976 is a combination electronic voltohmmeter, milliammeter and capacitance analyzer.

After searching for a PDF manual for this meter without success, I finally decided to scan the hard copy I received from my grandpa, a retired FAA radio technician. The manual is dated June 28, 1950, and appears to have been published by the [Civil Aeronautics Administration](https://en.wikipedia.org/wiki/United_States_government_role_in_civil_aviation#Civil_Aeronautics_Authority). As far as I am able to tell, the manual is now in the Public Domain. There are no copyright notices, and it appears to have been written as part of a contract with the CAA.

Folded in the front of my copy of the manual was Electronic Equipment Modification No. 122 (EEM 122), dated February 1, 1955. EEM 122 contains instructions for how to modify the ohmmeter so that it can measure leakage resistance with up to 480V DC in lieu of the 1.5-3V battery normally used in resistance measurements.

I used the following method to generate the PDF files:
1. Clean up the scans in GIMP
   - White-point adjustment: whiten the background, removes paper texture and discoloration
   - Remove artifacts such as page edges, hole punch marks, and creases
2. Use [unpaper](https://github.com/Flameeyes/unpaper) to slightly rotate and center the text
3. Perform OCR using [tesseract](https://tesseract-ocr.github.io/)
4. Add chapter divisions and page numbering using [ghostscript](https://www.ghostscript.com/index.html)

Note that Steps 2-4 have been automated in a GNU Makefile. It is strongly suggested that make be run in parallel mode (`make -j 8 ...`), as it will significantly speed up the build process.

Release Files
-------------

- [Weston_Model_976.pdf](https://calebreister.gitlab.io/weston_model_976/manual.pdf)
- [Schematic.pdf](https://calebreister.gitlab.io/weston_model_976/schematic.pdf)
- [EEM122.pdf](https://calebreister.gitlab.io/weston_model_976/eem122.pdf)
