PAGES := $(wildcard scan/*.xcf)
SKIP  := scan/Cover.xcf \
		 scan/Schematic.xcf \
		 scan/EEM122_02.xcf \
		 scan/EEM122_04.xcf \
		 scan/EEM122_09.xcf \
		 scan/Page_43.xcf \
		 scan/Page_44.xcf \
		 scan/Page_45.xcf \
		 scan/Page_46.xcf \
		 scan/Page_47.xcf \
		 scan/Page_48.xcf

UNPAPER = unpaper -q --overwrite --no-grayfilter $(UNPAPER_FLAGS) $< $@
UNPAPER_PAGES := $(patsubst scan/%.xcf, unpaper/%.pgm, $(PAGES))

export OMP_THREAD_LIMIT := 1
TESSERACT_FLAGS := --dpi 300 --oem 2
OCR_PAGES := $(patsubst unpaper/%.pgm, ocr/%.pdf, $(UNPAPER_PAGES))
SECTION = sed -nr '/(SECTION\s+[IV]+)$$/{s/$$/ - /; N; N; s/\n+//p}' $(@:.pdf=.txt)

.PHONY: all unpaper ocr
all: EEM122.pdf Weston_Model_976.pdf Schematic.pdf
unpaper: $(UNPAPER_PAGES)
ocr: $(OCR_PAGES)

EEM122.pdf: $(filter ocr/EEM122_%.pdf,$(OCR_PAGES)) EEM122.pdfmarks
	gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -o $@ -f $^

Weston_Model_976.pdf: ocr/Front.pdf ocr/Pages.pdf Weston_Model_976.pdfmarks
	gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -o $@ -f $^

Schematic.pdf: ocr/Schematic.pdf Schematic.pdfmarks
	gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -o $@ -f $^

ocr/Front.pdf: ocr/Cover.pdf $(filter ocr/Front_%.pdf,$(OCR_PAGES))
	gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -o $@ \
	   -c "[/_objdef {pl} /type /dict /OBJ pdfmark" \
	   -c "[{pl} <</Nums [ 0 << /P (cover) >> 1 << /S /R >> 9 << /S /D >> ] >>" \
	   -c " /PUT pdfmark [{Catalog} <</PageLabels {pl}>> /PUT pdfmark" \
	   -c "[/Page 5 /Title (TABLE OF CONTENTS) /OUT pdfmark" \
	   -f $^

ocr/Pages.pdf: $(filter ocr/Page_%.pdf,$(OCR_PAGES))
	gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -o $@ -f $^

ocr/Page_%.pdf: unpaper/Page_%.pgm
	tesseract $(TESSERACT_FLAGS) $< $(basename $@) pdf txt quiet
	mv $@ $@~
	gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -o $@ \
	   -c "[/Page 1 /Title ($$($(SECTION))) /OUT pdfmark" \
	   -f $@~
ocr/%.pdf: unpaper/%.pgm
	tesseract $(TESSERACT_FLAGS) $< $(basename $@) pdf txt quiet

unpaper/Front_%.pgm:   UNPAPER_FLAGS := -dr 2.0 -mt 0.05
unpaper/%.pgm:         UNPAPER_FLAGS := -dr 2.0 -mt 0.065
unpaper/Front_VII.pgm: UNPAPER_FLAGS := --no-deskew
unpaper/Page_13.pgm:   UNPAPER_FLAGS := --no-deskew -mt 0.05
unpaper/%.pgm: scan/%.pgm
	$(if $(filter $<,$(SKIP:.xcf=.pgm)), cp $< $@, $(UNPAPER))

scan/%.pgm: scan/%.xcf
	convert $< $@

.PHONY: clean clean-unpaper clean-ocr
clean: clean-unpaper clean-ocr
clean-unpaper:
	rm -f unpaper/*
clean-ocr:
	rm -rf ocr/*
